FROM alpine:3.14
RUN mkdir /app
COPY pocketbase /app
CMD ["/app/pocketbase", "serve", "--http", "0.0.0.0:8090"]
